module.exports = (path, modules) => {
  const obj = {}

  modules.forEach(module => {
    obj[module] = `${path}/${module}`
  })

  return obj
}
