const generateAliases = require('./generate-aliases')
const getModulesList = require('./get-modules-list')

module.exports = {
  generateAliases,
  getModulesList
}
