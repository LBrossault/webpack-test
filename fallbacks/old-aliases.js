const path = require('path')

const constants = require('../utils/constants')

module.exports = {
  img: path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-library/assets/img`
  ),
  styles: path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-library/styles`
  ),
  fonts: path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-library/assets/fonts`
  ),
  '~insideApp': path.resolve('.'),
  '~insideAdministration': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-administration`
  ),
  '~insideCore': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-core`
  ),
  '~instanceCustom': path.resolve(
    constants.context,
    './inside-customs'
  ),
  '~insideDisplays': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-displays`
  ),
  '~insideForms': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-forms`
  ),
  '~insideLibrary': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-library`
  ),
  '~insideEdition': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-edition`
  ),
  '~instanceConf': path.resolve(constants.context, './conf'),
  '~insidePagebuilder': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-pagebuilder`
  ),
  '~insideUsers': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-users`
  ),
  '~insideTests': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-tests`
  ),
  '~insideVendors': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-library/vendors`
  ),
  '~insideWebpack': path.resolve(
    constants.context,
    `${constants.modulesPath}/inside-webpack`
  ),
  '~moduleDirectory': path.resolve(
    constants.context,
    constants.modulesPath
  )
}
