const fs = require('fs')
const excludeList = [
  '.DS_Store',
  'inside-tests',
  'inside-webpack',
  'inside-webpack-v2'
]

module.exports = path => fs.readdirSync(path).filter(module => !excludeList.includes(module))
