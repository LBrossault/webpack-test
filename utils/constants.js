const path = require('path')
const fs = require('fs')

const isFactory = fs.existsSync(path.resolve(__dirname, '../../../../factory_modules'))
const { name } = require(path.resolve(__dirname, '../../../../package.json'))

module.exports = {
  appName: name,
  context: path.resolve(__dirname, '../../../..'),
  isFactory,
  isDevMode: process.env.WEBPACK_DEV_SERVER,
  modulesPath: path.resolve(__dirname, `../../../../${isFactory ? 'factory_modules' : 'node_modules'}/@maecia`),
  customPath: path.resolve(__dirname, '../../../..', './inside-customs'),
  insideConfig: {
    /**
     * Default config
     */
    ...require('../inside.config.json'),
    /**
     * Custom config
     */
    ...require(path.resolve(__dirname, '../../../..', './inside-customs/inside.config.json'))['inside-webpack']
  }
}
