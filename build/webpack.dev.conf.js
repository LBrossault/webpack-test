const confMerge = require('webpack-merge')

const constants = require('../utils/constants')

/**
 * Plugins
 */
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

/**
 * Webpack base config
 */
const insideDevConfig = constants.insideConfig.develop
const webpackBase = require('./webpack.base.conf')

module.exports = confMerge(webpackBase, {
  mode: 'development',
  devtool: 'eval-source-map',
  devServer: {
    clientLogLevel: 'silent',
    compress: true,
    historyApiFallback: true,
    hot: true,
    open: !insideDevConfig.useBrowserSync,
    overlay: true,
    quiet: true
  },
  plugins: [
    new FriendlyErrorsWebpackPlugin(),
    /**
     * Enable Browsersync if it's specified in the inside.config
     */
    ...(insideDevConfig.useBrowserSync ? [new BrowserSyncPlugin({
      host: 'localhost',
      port: 8080,
      proxy: 'http://localhost:8080/'
    })] : []),
    /**
     * Enable Bundle analyser if it's specified in the inside.config
     */
    ...(insideDevConfig.useBundleAnalyser ? [new BundleAnalyzerPlugin()] : []),
    /**
     * Allow to use index.html for development
     */
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.html',
      inject: true
    })
  ]
})
