const fs = require('fs')
const path = require('path')

const constants = require('../utils/constants')

function InsideGlobals() {
  this.customPath = constants.customPath
}

InsideGlobals.prototype.getFile = function(
  elementPath,
  isFolder,
  defaultValue = {}
) {
  if (fs.existsSync(path.resolve(this.customPath, elementPath))) {
    /*
     * Parse all files in folder and return an object with a key for each file
     */
    if (isFolder) {
      const obj = {}
      const files = fs.readdirSync(path.resolve(this.customPath, elementPath))

      for (let y = 0; y < files.length; y++) {
        if (files[y].indexOf('.js') > -1) {
          obj[files[y].replace('.js', '')] = require(path.resolve(
            this.customPath,
            elementPath,
            files[y]
          ))
        }
      }

      return obj
    } else {
      /*
       * Return the file directly
       */
      return require(path.resolve(this.customPath, elementPath))
    }
  } else {
    return defaultValue
  }
}

InsideGlobals.prototype.getGlobals = function() {
  /*
   * Add all variables
   */
  return {
    settings: this.getFile('settings/index.js', false, {
      groups: [],
      fields: [],
      disabled: []
    })
  }
}

const singleton = new InsideGlobals()

Object.freeze(singleton)

module.exports = singleton
