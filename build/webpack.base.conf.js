'use strict'

const path = require('path')
const webpack = require('webpack')

/**
 * Plugins
 */
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

const constants = require('../utils/constants')
const helpers = require('../helpers')
const InsideGlobalSettings = require('../classes/InsideGlobalSettings')

const aliases = helpers.generateAliases(
  constants.modulesPath,
  helpers.getModulesList(constants.modulesPath)
)
const fallbackAliases = require('../fallbacks/old-aliases')

module.exports = {
  context: constants.context,
  entry: {
    polyfills: 'babel-polyfill',
    ...aliases,
    app: path.join(constants.context, './index.js')
  },
  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm.js',
      ...aliases,
      ...fallbackAliases
    },
    extensions: ['.js', '.vue', '.json', '.scss']
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /node_modules(?!(\/|\\)@maecia).*/,
          name: 'vendor',
          chunks: 'initial'
        }
      }
    }
  },
  module: {
    unknownContextCritical: false,
    rules: [
      /**
       * JS
       */
      {
        enforce: 'pre',
        test: /\.(js|vue)$/,
        exclude: /(node_modules|vendors)/,
        loader: 'eslint-loader',
        options: {
          configFile: '.eslintrc.js',
          cache: false,
          formatter: require('eslint-friendly-formatter')
        }
      },
      {
        test: /\.js?$/,
        use: [
          'babel-loader',
          ...(constants.isDevMode ? [{
            loader: 'thread-loader',
            options: {
              workers: 2,
              workerParallelJobs: 50,
              workerNodeArgs: ['--max-old-space-size=1024'],
              poolRespawn: false,
              poolTimeout: 2000,
              poolParallelJobs: 50
            }
          }] : [])
        ],
        exclude: /node_modules(?!(\/|\\)@maecia).*/
      },
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      /**
       * Styles
       */
      {
        test: /\.(scss|css)$/,
        use: [
          ...(!constants.isDevMode ? [{
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '../'
            }
          }] : ['vue-style-loader']),
          'css-loader',
          'sass-loader',
          {
            loader: 'sass-resources-loader',
            options: {
              /**
               * Auto inject scss variables and mixins in all components
               */
              resources: [
                path.join(
                  constants.modulesPath,
                  'inside-library/styles/variables/variables.scss'
                ),
                path.join(
                  constants.modulesPath,
                  'inside-library/styles/mixins/mixins.scss'
                ),
                path.join(
                  constants.context,
                  'inside-customs/styles/variables/variables-instance.scss'
                )
              ]
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              config: {
                path: path.join(constants.context, './.postcssrc.js')
              }
            }
          }
        ]
      },
      /**
       * Assets
       */
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        loader: 'file-loader',
        exclude: /fonts/,
        options: {
          name: 'img/[name][hash].[ext]'
        }
      },
      {
        test: /\.(svg|woff|woff2|ttf)$/i,
        loader: 'file-loader',
        exclude: /img/,
        options: {
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  },
  plugins: [
    /**
     * Allow to define global variables
     */
    new webpack.DefinePlugin({
      'process.env': require('../global-variables/dev.js'),
      inside: JSON.stringify(InsideGlobalSettings.getGlobals())
    }),
    new VueLoaderPlugin()
  ]
}
