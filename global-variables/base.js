const constants = require('../utils/constants')

const confEnv = require(`${constants.context}/conf/conf.env.js`)
const confSite = require(`${constants.context}/conf/conf.site.js`)

module.exports = {
  APPNAME: JSON.stringify(constants.appName),
  CONFPATH: JSON.stringify(`${constants.context}/conf`),
  ROOTPATH: JSON.stringify(constants.context),
  GLOBAL: JSON.stringify({
    ...confSite,
    ...confEnv
  })
}
