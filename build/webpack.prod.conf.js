const confMerge = require('webpack-merge')
const path = require('path')

const constants = require('../utils/constants')

const CompressionWebpackPlugin = require('compression-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const ImageminPlugin = require('imagemin-webpack-plugin').default
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const WebpackBar = require('webpackbar')

const player = require('play-sound')(opts = {})
const WebpackBuildNotifierPlugin = require('webpack-build-notifier')

/**
 * Webpack base config
 */
const insideProdConfig = constants.insideConfig.production
const webpackBase = require('./webpack.base.conf')

module.exports = confMerge(webpackBase, {
  mode: 'production',
  devtool: 'source-map',
  stats: 'minimal',
  optimization: {
    minimizer: [new TerserPlugin({
      chunkFilter: chunk => chunk.name !== 'vendor',
      terserOptions: {
        compress: {
          drop_console: true
        }
      }
    })]
  },
  output: {
    path: path.resolve(constants.context, insideProdConfig.exportPath),
    filename: `js/[name].js?t=${new Date().getTime()}`,
    chunkFilename: `js/[name].js?t=${new Date().getTime()}`,
    publicPath: 'themes/custom/inside-drupal-theme/assets/'
  },
  performance: {
    hints: false
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CompressionWebpackPlugin(),
    new ImageminPlugin({
      pngquant: {
        quality: '95-100'
      }
    }),
    new MiniCssExtractPlugin({
      filename: 'styles/[name].css',
      chunkFilename: 'styles/[id].css'
    }),
    new WebpackBar(),
    new WebpackBuildNotifierPlugin({
      title: constants.appName,
      successSound: false,
      failureSound: false,
      compilationSound: false,
      warningSound: false,
      suppressSuccess: true,
      onComplete: (compilation, status) => {
        if (insideProdConfig.playSoundOnBuild) {
          switch (status) {
            case 'success':
              player.play(path.resolve(__dirname, '../utils/sounds/success.mp3'))
              break
            case 'error':
              player.play(path.resolve(__dirname, '../utils/sounds/error.mp3'))
              break
          }
        }
      }
    })
  ]
})
