const commonVariables = require('./base')

module.exports = {
  ...commonVariables,
  NODE_ENV: JSON.stringify('production'),
}
